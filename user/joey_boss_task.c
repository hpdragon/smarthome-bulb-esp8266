/*
 * joey_boss_task.c
 *
 *  Created on: Aug 19, 2017
 *      Author: hpdragon
 */

#include "joey_boss_task.h"

#include "mailbox.h"
#include "marky_network_task.h"
#include "deedee_light_task.h"

xTaskHandle JOEYT_Init(void *pArg)
{
	xTaskHandle joey;

	xTaskCreate(JOEYT_Task, "JOEYT_Task", configMINIMAL_STACK_SIZE, pArg, tskIDLE_PRIORITY, joey);

	return joey;
}

void JOEYT_Task(void *pArg)
{
	//task's mailbox
	Task_Mailbox mailbox;

	//init light task
	DEEDEET_Init(&mailbox);
	//init network task
	MARKYT_Init(&mailbox);

	while(1)
	{
//		printf("joey task!\n");

		vTaskDelay(100);
	}
}

