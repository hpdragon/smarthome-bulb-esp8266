/*
 * deedee_light_task.c
 *
 *  Created on: Aug 19, 2017
 *      Author: hpdragon
 */

#include "deedee_light_task.h"

#include "esp8266_arduino_ws2812.h"

xTaskHandle DEEDEET_Init(void *pArg)
{
	xTaskHandle deedee;

	xTaskCreate(DEEDEET_Task, "DEEDEET_Task", configMINIMAL_STACK_SIZE*2, pArg, tskIDLE_PRIORITY, deedee);

	return deedee;
}

void DEEDEET_Task(void *pArg)
{
	//task's mailbox
	Task_Mailbox* mailbox;
	//light data
	Task_Mailbox_DEEDEE oldLightData[6];

	//get the task's mailbox
	mailbox = (Task_Mailbox*)pArg;

	//init ws2812
	WS2812_Init(16, 5, NEO_GRB + NEO_KHZ800);
	WS2812_Begin();

	//light up first
//	mailbox->deedee = 5255255255255255UL;
	mailbox->deedee[0] = 255, mailbox->deedee[1] = 255, mailbox->deedee[2] = 255, mailbox->deedee[3] = 255, mailbox->deedee[4] = 255, mailbox->deedee[5] = 5;
	DEEDEE_UpdateLight(mailbox->deedee);
	DEEDEE_CopyLight(oldLightData, mailbox->deedee);

	while(1)
	{
		if(DEEDEE_Light_isDiff(oldLightData, mailbox->deedee))
		{
			DEEDEE_UpdateLight(mailbox->deedee);

//			oldLightData = mailbox->deedee;
			DEEDEE_CopyLight(oldLightData, mailbox->deedee);
		}

		vTaskDelay(10);
	}
}

void DEEDEE_CopyLight(Task_Mailbox_DEEDEE* light1, Task_Mailbox_DEEDEE* light2)
{
	int i = 0;
	for(i = 0; i < 6; i++)
		light1[i] = light2[i];
}

bool DEEDEE_Light_isDiff(Task_Mailbox_DEEDEE* light1, Task_Mailbox_DEEDEE* light2)
{
	int i = 0;
	for(i = 0; i < 6; i++)
		if(light1[i] != light2[i])
			return true;

	return false;
}

void DEEDEE_UpdateLight(Task_Mailbox_DEEDEE* light)
{
	//for loop
	int i = 0;
	//lightData
//	uint8_t lightData[5];

//	lightData[0] = (uint8_t)((light % 1000000000000000UL)/1000000000000UL);
//	lightData[1] = (uint8_t)((light % 1000000000000UL)/1000000000UL);
//	lightData[2] = (uint8_t)((light % 1000000000UL)/1000000UL);
//	lightData[3] = (uint8_t)((light % 1000000UL)/1000UL);
//	lightData[4] = (uint8_t)((light % 1000UL)/1UL);

	//analyze light data
	//light: red green blue white brightness
	for(i=0; i<16; i++)
		// Moderately bright green color.
		WS2812_SetPixelColor( i, light[4], light[3], light[2], light[1] );
	WS2812_SetBrightness( light[0] );

	// This sends the updated pixel color to the hardware.
	//FIXME	the fucking problem for calling WS2812_Show() twice, if call it once, led #1 will light green (mabey because of epp8266_arduino_ws2812 library WS2812_Show() for particular
	WS2812_Show();
	WS2812_Show();
}
