/*
 * joey_boss_task.h
 *
 *  Created on: Aug 19, 2017
 *      Author: hpdragon
 */

#ifndef INCLUDE_JOEY_BOSS_TASK_H_
#define INCLUDE_JOEY_BOSS_TASK_H_

#include "esp_common.h"
#include "freertos/FreeRTOS.h"

//-----------------------------------------------------------------------



//-----------------------------------------------------------------------

/*
 *@brief	init joey (boss) task
 *@param	pArg:	this will be passed to joey task (JOEY_Task) as a argument
 *@return
 *	- joey handler (task handler)
 */
xTaskHandle JOEYT_Init(void *pArg);

/*
 *@brief	main joey task
 *@param	pArg:	argument for handler stuff (optional)
 */
void JOEYT_Task(void *pArg);

#endif /* INCLUDE_JOEY_BOSS_TASK_H_ */
