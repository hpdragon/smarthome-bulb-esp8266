/*
 * mailbox.h
 *
 *  Created on: Aug 20, 2017
 *      Author: hpdragon
 */

#ifndef INCLUDE_MAILBOX_H_
#define INCLUDE_MAILBOX_H_

#include "esp_common.h"

//-----------------------------------------------------------------------

/*
 * @brief	each task mail box data type
 */
typedef uint16_t Task_Mailbox_Marky;
typedef uint16_t Task_Mailbox_JOEY;
typedef uint8_t Task_Mailbox_DEEDEE;

/*
 * @brief	task's mailbox
 */
typedef struct TASK_MAILBOX
{
	Task_Mailbox_Marky marky;				/*!< marky mailbox */
	Task_Mailbox_JOEY joey;					/*!< joey mailbox */
	//TODO	improve this shitty communication style
	Task_Mailbox_DEEDEE deedee[6];			/*!< deedee mailbox */
} Task_Mailbox;

//-----------------------------------------------------------------------



#endif /* INCLUDE_MAILBOX_H_ */
