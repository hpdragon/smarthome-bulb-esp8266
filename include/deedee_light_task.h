/*
 * deedee_light_task.h
 *
 *  Created on: Aug 19, 2017
 *      Author: hpdragon
 */

#ifndef INCLUDE_DEEDEE_LIGHT_TASK_H_
#define INCLUDE_DEEDEE_LIGHT_TASK_H_

#include "esp_common.h"
#include "freertos/FreeRTOS.h"

#include "mailbox.h"

/*
 *@brief	init deedee (light) task
 *@param	pArg:	this will be passed to deedee task (DEEDEE_Task) as a argument
 *@return
 *	- deedee handler (task handler)
 */
xTaskHandle DEEDEET_Init(void *pArg);

/*
 *@brief	main deedee task
 *@param	pArg:	argument for handler stuff (optional)
 */
void DEEDEET_Task(void *pArg);

/*
 * @brief	copy light
 * @param	light1:		destination of copy
 * @param	light2:		source of copy
 */
void DEEDEE_CopyLight(Task_Mailbox_DEEDEE* light1, Task_Mailbox_DEEDEE* light2);

/*
 * @brief	compare two light
 * @param	light1:		light to compare
 * @param	light2:		light to compare
 * @return	true if two light is different
 */
bool DEEDEE_Light_isDiff(Task_Mailbox_DEEDEE* light1, Task_Mailbox_DEEDEE* light2);

/*
 * @brief	update light
 * @param	light to update (array of 6 element)
 * 			- exp: [red, green, blue, white, brightness]
 */
void DEEDEE_UpdateLight(Task_Mailbox_DEEDEE* light);

#endif /* INCLUDE_DEEDEE_LIGHT_TASK_H_ */
